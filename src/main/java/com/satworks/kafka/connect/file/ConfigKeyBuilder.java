package com.satworks.kafka.connect.file;

import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import java.util.Collections;
import java.util.List;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigDef.ConfigKey;
import org.apache.kafka.common.config.ConfigDef.Importance;
import org.apache.kafka.common.config.ConfigDef.Recommender;
import org.apache.kafka.common.config.ConfigDef.Type;
import org.apache.kafka.common.config.ConfigDef.Validator;
import org.apache.kafka.common.config.ConfigDef.Width;

public class ConfigKeyBuilder {
    private final String name;
    private final Type type;
    private String documentation;
    private Object defaultValue;
    private Validator validator;
    private Importance importance;
    private String group;
    private int orderInGroup;
    private Width width;
    private String displayName;
    private List<String> dependents;
    private Recommender recommender;
    private boolean internalConfig;

    private ConfigKeyBuilder(String group, String name, Type type) {
        this(name, type);
        this.group = group;
    }

    private ConfigKeyBuilder(String name, Type type) {
        this.documentation = "";
        this.defaultValue = ConfigDef.NO_DEFAULT_VALUE;
        this.group = "";
        this.orderInGroup = -1;
        this.width = Width.NONE;
        this.dependents = Collections.emptyList();
        this.internalConfig = true;
        this.name = name;
        this.displayName = name;
        this.type = type;
    }

    public static ConfigKeyBuilder of(String group, String name, Type type) {
        return new ConfigKeyBuilder(group, name, type);
    }

    public static ConfigKeyBuilder of(String name, Type type) {
        return new ConfigKeyBuilder(name, type);
    }

    public ConfigKey build() {
        Preconditions.checkState(!Strings.isNullOrEmpty(this.name), "name must be specified.");
        return new ConfigKey(this.name, this.type, this.defaultValue, this.validator, this.importance, this.documentation, this.group, this.orderInGroup, this.width, this.displayName, this.dependents, this.recommender, this.internalConfig);
    }

    public String name() {
        return this.name;
    }

    public Type type() {
        return this.type;
    }

    public String documentation() {
        return this.documentation;
    }

    public ConfigKeyBuilder documentation(String documentation) {
        this.documentation = documentation;
        return this;
    }

    public Object defaultValue() {
        return this.defaultValue;
    }

    public ConfigKeyBuilder defaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
        return this;
    }

    public Validator validator() {
        return this.validator;
    }

    public ConfigKeyBuilder validator(Validator validator) {
        this.validator = validator;
        return this;
    }

    public Importance importance() {
        return this.importance;
    }

    public ConfigKeyBuilder importance(Importance importance) {
        this.importance = importance;
        return this;
    }

    public String group() {
        return this.group;
    }

    public ConfigKeyBuilder group(String group) {
        this.group = group;
        return this;
    }

    public int orderInGroup() {
        return this.orderInGroup;
    }

    public ConfigKeyBuilder orderInGroup(int orderInGroup) {
        this.orderInGroup = orderInGroup;
        return this;
    }

    public Width width() {
        return this.width;
    }

    public ConfigKeyBuilder width(Width width) {
        this.width = width;
        return this;
    }

    public String displayName() {
        return this.displayName;
    }

    public ConfigKeyBuilder displayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public List<String> dependents() {
        return this.dependents;
    }

    public ConfigKeyBuilder dependents(List<String> dependents) {
        this.dependents = dependents;
        return this;
    }

    public ConfigKeyBuilder dependents(String... dependents) {
        return this.dependents((List) ImmutableList.copyOf(dependents));
    }

    public Recommender recommender() {
        return this.recommender;
    }

    public ConfigKeyBuilder recommender(Recommender recommender) {
        this.recommender = recommender;
        return this;
    }

    public boolean isinternalConfig() {
        return this.internalConfig;
    }

    public ConfigKeyBuilder internalConfig(boolean internalConfig) {
        this.internalConfig = internalConfig;
        return this;
    }
}
