package com.satworks.kafka.connect.file;

import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

class InputFile implements Closeable {
    private static final Logger log = LoggerFactory.getLogger(InputFile.class);
    public final File inputFile;
    public final File processingFlag;
    public InputStream inputStream;

    InputFile(File inputFile, File processingFlag) {
        this.inputFile = inputFile;
        this.processingFlag = processingFlag;
    }

    public InputStream openStream() throws IOException {
        if (null != this.inputStream) {
            throw new IOException(
                    String.format("File %s is already open", this.inputFile)
            );
        }

        final String extension = Files.getFileExtension(inputFile.getName());
        log.trace("read() - fileName = '{}' extension = '{}'", inputFile, extension);
        final InputStream fileInputStream = new FileInputStream(this.inputFile);
        inputStream = fileInputStream;

        log.info("Creating processing flag {}", this.processingFlag);
        Files.touch(this.processingFlag);

        return inputStream;
    }

    @Override
    public String toString() {
        return this.inputFile.toString();
    }

    public void close() throws IOException {
        if (null != this.inputStream) {
            log.info("Closing {}", this.inputFile);
            this.inputStream.close();
        }
        if (this.processingFlag.exists()) {
            log.info("Removing processing flag {}", this.processingFlag);
            if (!this.processingFlag.delete()) {
                log.warn("Could not remove processing flag {}", this.processingFlag);
            }
        }
    }
}
