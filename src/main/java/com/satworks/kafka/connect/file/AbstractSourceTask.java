package com.satworks.kafka.connect.file;

import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableMap;
import org.apache.kafka.connect.data.SchemaAndValue;
import org.apache.kafka.connect.errors.ConnectException;
import org.apache.kafka.connect.source.SourceRecord;
import org.apache.kafka.connect.source.SourceTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public abstract class AbstractSourceTask<CONF extends BaseConfig> extends SourceTask {
    private static final Logger log = LoggerFactory.getLogger(AbstractSourceTask.class);
    private final Stopwatch processingTime = Stopwatch.createUnstarted();
    protected Map<String, ?> sourcePartition;
    protected CONF config;
    protected InputFile inputFile;
    protected long inputFileModifiedTime;
    protected Map<String, String> metadata;
    int emptyCount = 0;
    long recordCount;
    private boolean hasRecords = false;
    InputFileDequeue inputFileDequeue;

    protected abstract CONF config(Map<String, ?> settings);

    protected abstract void configure(InputStream inputStream, Map<String, String> metadata, Long lastOffset) throws IOException;

    protected abstract List<SourceRecord> process() throws IOException;

    protected abstract long recordOffset();

    @Override
    public void start(Map<String, String> settings) {
        this.config = config(settings);
    }

    @Override
    public void stop() {
        log.info("Stopping task.");
        try {
            if (null != this.inputFile) {
                this.inputFile.close();
            }
        } catch (IOException ex) {
            log.error("Exception thrown while closing {}", this.inputFile);
        }
    }

    public String version() {
        return this.getClass().getPackage().getImplementationVersion();
    }

    @Override
    public List<SourceRecord> poll() throws InterruptedException {
        log.trace("poll()");
        List<SourceRecord> results = read();

        if (results.isEmpty()) {
            emptyCount++;
            if (emptyCount > 1) {
                log.trace("read() returned empty list. Sleeping {} ms.", this.config.emptyPollWaitMs);
                Thread.sleep(this.config.emptyPollWaitMs);
            }
            return null;
        }
        emptyCount = 0;
        log.trace("read() returning {} result(s)", results.size());

        return results;
    }

    private void recordProcessingTime() {
        log.info(
                "Finished processing {} record(s) in {} second(s).",
                this.recordCount,
                processingTime.elapsed(TimeUnit.SECONDS)
        );
    }

    public List<SourceRecord> read() {
        try {
            if (!hasRecords) {

                if (null != this.inputFile) {
                    recordProcessingTime();
                    this.inputFile.close();
                    this.inputFile = null;
                }

                log.trace("read() - polling for next file.");
                InputFile nextFile = this.inputFileDequeue.poll();

                log.trace("read() - nextFile = '{}'", nextFile);
                if (null == nextFile) {
                    log.trace("read() - No next file found.");
                    return new ArrayList<>();
                }

                this.metadata = ImmutableMap.of();
                this.inputFile = nextFile;
                this.inputFileModifiedTime = this.inputFile.inputFile.lastModified();

                try {
                    this.inputFile.openStream();
                    this.sourcePartition = ImmutableMap.of(
                            "fileName", this.inputFile.inputFile.getName()
                    );
                    log.info("Opening {}", this.inputFile);
                    Long lastOffset = null;
                    log.trace("looking up offset for {}", this.sourcePartition);
                    Map<String, Object> offset = this.context.offsetStorageReader().offset(this.sourcePartition);
                    if (null != offset && !offset.isEmpty()) {
                        Number number = (Number) offset.get("offset");
                        lastOffset = number.longValue();
                    }

                    this.recordCount = 0;
                    log.trace("read() - calling configure()");
                    configure(this.inputFile.inputStream, this.metadata, lastOffset);
                } catch (Exception ex) {
                    throw new ConnectException(ex);
                }
                processingTime.reset();
                processingTime.start();
            }
            List<SourceRecord> records = process();
            this.hasRecords = !records.isEmpty();
            return records;
        } catch (Exception ex) {
            log.error("Exception encountered processing line {} of {}.", recordOffset(), this.inputFile, ex);
            if (this.config.haltOnError) {
                throw new ConnectException(ex);
            } else {
                return new ArrayList<>();
            }
        }
    }

    protected Map<String, ?> offset() {
        return ImmutableMap.of(
                "offset",
                recordOffset()
        );
    }

    protected SourceRecord record(
            SchemaAndValue key,
            SchemaAndValue value,
            Long timestamp) {
        Map<String, ?> sourceOffset = ImmutableMap.of(
                "offset",
                recordOffset()
        );

        return new SourceRecord(
                this.sourcePartition,
                sourceOffset,
                this.config.topic,
                null,
                null != key ? key.schema() : null,
                null != key ? key.value() : null,
                value.schema(),
                value.value(),
                timestamp
        );
    }
}
