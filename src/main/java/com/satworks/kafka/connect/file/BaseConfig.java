package com.satworks.kafka.connect.file;

import com.google.common.io.PatternFilenameFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.common.config.AbstractConfig;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.SchemaBuilder;

import java.io.File;
import java.util.Map;
import java.util.regex.Pattern;

public abstract class BaseConfig extends AbstractConfig {
    private static final String INPUT_PATH_CONFIG = "input.path";
    private static final String FINISHED_PATH_CONFIG = "finished.path";
    private static final String ERROR_PATH_CONFIG = "error.path";
    public static final String INPUT_FILE_PATTERN_CONF = "input.file.pattern";
    private static final String HALT_ON_ERROR_CONF = "halt.on.error";
    private static final String BATCH_SIZE_CONF = "batch.size";
    public static final String PROCESSING_FILE_EXTENSION_DEFAULT = ".PROCESSING";
    private static final String TOPIC_CONF = "topic";
    private static final String SYSTEM = "system";
    private static final String EMPTY_POLL_WAIT_MS_CONF = "empty.poll.wait.ms";
    private static final String SCHEMA_FROM_HEADER = "schema.from.header";
    private static final String SCHEMA_NAME = "schema.name";
    private static final String KEY_FIELD = "key.field";
    private static final String ADDITIONAL_SCHEMA_FIELDS = "additional.schema.fields";
    private static final String EVENT_TOPIC = "event.topic";
    private static final String DATE_FROM_FILE_NAME = "date.from.file.name";
    private static final String FILE_NAME_DATE_PATTERN = "file.name.date.format";
    private static final String STATIC_HEADERS = "static.headers";
    private static final String EVENT_SCHEMA_NAME = "com.kafka.connect.Event";

    public final File inputPath;
    public final File finishedPath;
    public final File errorPath;
    public final boolean haltOnError;
    public final boolean schemaFromHeader;
    public final int batchSize;
    public final String topic;
    public final String system;
    public final String schemaNane;
    public final String keyField;
    public final long emptyPollWaitMs;
    public final PatternFilenameFilter inputFilenameFilter;
    public final String additionalSchemaFields;
    public final String eventTopic;
    public final boolean streamDateFromFile;
    public final String fileNameDatePattern;
    public final String staticHeaders;

    public final Schema eventSchema;

    public BaseConfig(ConfigDef definition, Map<?, ?> originals) {
        super(definition, originals);
        this.inputPath = new File(this.getString(INPUT_PATH_CONFIG));
        this.finishedPath = new File(this.getString(FINISHED_PATH_CONFIG));
        this.errorPath = new File(this.getString(ERROR_PATH_CONFIG));
        this.haltOnError = this.getBoolean(HALT_ON_ERROR_CONF);
        this.batchSize = this.getInt(BATCH_SIZE_CONF);
        this.topic = this.getString(TOPIC_CONF);
        this.emptyPollWaitMs = this.getLong(EMPTY_POLL_WAIT_MS_CONF);
        final String inputPatternText = this.getString(INPUT_FILE_PATTERN_CONF);
        final Pattern inputPattern = Pattern.compile(inputPatternText);
        this.inputFilenameFilter = new PatternFilenameFilter(inputPattern);
        this.system = this.getString(SYSTEM);
        this.schemaFromHeader = this.getBoolean(SCHEMA_FROM_HEADER);
        this.schemaNane = this.getString(SCHEMA_NAME);
        this.keyField = this.getString(KEY_FIELD);
        this.additionalSchemaFields = this.getString(ADDITIONAL_SCHEMA_FIELDS);
        this.eventTopic = this.getString(EVENT_TOPIC);
        this.streamDateFromFile = this.getBoolean(DATE_FROM_FILE_NAME);
        this.fileNameDatePattern = this.getString(FILE_NAME_DATE_PATTERN);
        this.staticHeaders = this.getString(STATIC_HEADERS);

        eventSchema = SchemaBuilder.struct()
                .name(EVENT_SCHEMA_NAME)
                .field("name", Schema.STRING_SCHEMA).optional()
                .field("component", Schema.STRING_SCHEMA).optional()
                .field("system", Schema.STRING_SCHEMA).optional()
                .field("status", Schema.STRING_SCHEMA).optional()
                .field("timestamp", Schema.STRING_SCHEMA).optional()
                .field("fileName", Schema.STRING_SCHEMA).optional()
                .field("errorMessage", Schema.STRING_SCHEMA).optional()
                .field("data", Schema.STRING_SCHEMA).optional().build();
    }

    public static ConfigDef config() {

        return new ConfigDef()
                .define(TOPIC_CONF, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new ConfigDef.NonEmptyString(), ConfigDef.Importance.HIGH, "")
                .define(INPUT_PATH_CONFIG, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new FileValidator(), ConfigDef.Importance.HIGH, "")
                .define(ERROR_PATH_CONFIG, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new FileValidator(), ConfigDef.Importance.HIGH, "")
                .define(FINISHED_PATH_CONFIG, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new FileValidator(), ConfigDef.Importance.HIGH, "")
                .define(HALT_ON_ERROR_CONF, ConfigDef.Type.BOOLEAN, true, ConfigDef.Importance.HIGH, "")
                .define(BATCH_SIZE_CONF, ConfigDef.Type.INT, 2000, ConfigDef.Importance.HIGH, "")
                .define(EMPTY_POLL_WAIT_MS_CONF, ConfigDef.Type.LONG, 100L, ConfigDef.Importance.LOW, "")
                .define(SYSTEM, ConfigDef.Type.STRING, "", ConfigDef.Importance.LOW, "")
                .define(SCHEMA_FROM_HEADER, ConfigDef.Type.BOOLEAN, true, ConfigDef.Importance.HIGH, "")
                .define(SCHEMA_NAME, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new ConfigDef.NonEmptyString(), ConfigDef.Importance.HIGH, "")
                .define(KEY_FIELD, ConfigDef.Type.STRING, "", ConfigDef.Importance.MEDIUM, "")
                .define(ADDITIONAL_SCHEMA_FIELDS, ConfigDef.Type.STRING, "stream_date,system,key", ConfigDef.Importance.MEDIUM, "")
                .define(EVENT_TOPIC, ConfigDef.Type.STRING, ConfigDef.NO_DEFAULT_VALUE, new ConfigDef.NonEmptyString(), ConfigDef.Importance.HIGH, "")
                .define(DATE_FROM_FILE_NAME, ConfigDef.Type.BOOLEAN, false, ConfigDef.Importance.HIGH, "")
                .define(FILE_NAME_DATE_PATTERN, ConfigDef.Type.STRING, "", ConfigDef.Importance.MEDIUM, "")
                .define(STATIC_HEADERS, ConfigDef.Type.STRING, "", ConfigDef.Importance.MEDIUM, "");
    }

    public static class FileValidator implements ConfigDef.Validator {
        FileValidator() {
        }

        public void ensureValid(String name, Object input) {
            if (input == null || StringUtils.isEmpty((String) input)) {
                throw new ConfigException(name, "Cannot be null or empty.");
            }

            File file = new File((String) input);

            if (!file.exists()) {
                try {
                    file.mkdir();
                    file.setReadable(true);
                    file.setWritable(true);
                } catch (Exception e) {
                    throw new ConfigException(name, (String) input, e.getMessage());
                }
            }

            if (!file.isAbsolute()) {
                throw new ConfigException(name, String.format("File '%s' is not an absolute path.", file));
            } else {
                if (file.canWrite()) {
                    throw new ConfigException(name, String.format("File '%s' should be writable.", file));
                } else if (!file.canRead()) {
                    throw new ConfigException(name, String.format("File '%s' should be readable.", file));
                }
            }
        }
    }

}
