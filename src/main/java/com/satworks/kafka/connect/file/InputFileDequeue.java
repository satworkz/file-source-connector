package com.satworks.kafka.connect.file;

import com.google.common.collect.ForwardingDeque;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

public class InputFileDequeue extends ForwardingDeque<InputFile> {
    private static final Logger log = LoggerFactory.getLogger(InputFileDequeue.class);
    private final BaseConfig config;
    Deque<InputFile> files;

    public InputFileDequeue(BaseConfig config) {
        this.config = config;
    }

    static File processingFile(String processingFileExtension, File input) {
        String fileName = input.getName() + processingFileExtension;
        return new File(input.getParentFile(), fileName);
    }

    @Override
    protected Deque<InputFile> delegate() {
        if (null != files && !files.isEmpty()) {
            return files;
        }

        log.info("Searching for file(s) in {}", this.config.inputPath);
        File[] input = this.config.inputPath.listFiles(this.config.inputFilenameFilter);
        if (null == input || input.length == 0) {
            log.info("No files matching {} were found in {}", BaseConfig.INPUT_FILE_PATTERN_CONF, this.config.inputPath);
            return new ArrayDeque<>();
        }
        Arrays.sort(input, Comparator.comparing(File::getName));
        List<File> files = new ArrayList<>(input.length);
        files.addAll(Arrays.asList(input));

        Deque<InputFile> result = new ArrayDeque<>(files.size());

        for (File file : files) {
            File processingFile = processingFile(BaseConfig.PROCESSING_FILE_EXTENSION_DEFAULT, file);
            log.trace("Checking for processing file: {}", processingFile);

            if (processingFile.exists()) {
                log.debug("Skipping {} because processing file exists.", file);
                continue;
            }

            long fileAgeMS = System.currentTimeMillis() - file.lastModified();

            if (fileAgeMS < 0L) {
                log.warn("File {} has a date in the future.", file);
            }

            result.add(new InputFile(file, processingFile));
        }

        log.info("Found {} file(s) to process", result.size());
        return (this.files = result);
    }
}
