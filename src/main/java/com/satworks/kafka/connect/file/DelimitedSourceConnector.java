package com.satworks.kafka.connect.file;

import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.Task;
import org.apache.kafka.connect.source.SourceConnector;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DelimitedSourceConnector extends SourceConnector {

    private DelimitedSourceConnectorConfig config;
    private Map<String, String> settings;

    public String version() {
        return this.getClass().getPackage().getImplementationVersion();
    }

    public void start(Map<String, String> input) {
        this.config = new DelimitedSourceConnectorConfig(input);
        this.settings = input;

    }

    public Class<? extends Task> taskClass() {
        return null;
    }

    public List<Map<String, String>> taskConfigs(int i) {
        return Collections.singletonList(this.settings);
    }

    public void stop() {

    }

    public ConfigDef config() {
        return null;
    }
}
